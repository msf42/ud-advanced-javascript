# Asynchronous JavaScript

## 129: Section Overview

---

## 130-135 Completed in last course

---

## 136: Job Queue

```javascript
// Callback Queue - Task Queue
setTimeout(()=>{console.log('1', 'is the loneliest number')}, 0)
setTimeout(()=>{console.log('2', 'can be as bad as one')}, 10)

//2 Job Queue - Microtask Queue (higher priority than Callback Queue.)
Promise.resolve('hi').then((data)=> console.log('2', data))

//3
console.log('3','is a crowd')
/*
3 is a crowd
2 hi
1 is the loneliest number
2 can be as bad as one
*/
```

[image](/1570545931127.png)

---

## 137: Parallel, Sequence, and Race

```javascript
const promisify = (item, delay) =>
  new Promise((resolve) =>
    setTimeout(() =>
      resolve(item), delay));

const a = () => promisify('a', 100);
const b = () => promisify('b', 5000);
const c = () => promisify('c', 3000);

async function parallel() {
  const promises = [a(), b(), c()];
  const [output1, output2, output3] = await Promise.all(promises); // waits for all to finish, then returns them in the order we gave
  return `parallel is done: ${output1} ${output2} ${output3}`
}

async function race() {
  const promises = [a(), b(), c()];
  const output1 = await Promise.race(promises); // returns the first to complete
  return `race is done: ${output1}`;
}

async function sequence() {
  const output1 = await a(); // pauses until a completes
  const output2 = await b(); // pauses until b completes
  const output3 = await c(); // pauses until c completes
  return `sequence is done ${output1} ${output2} ${output3}`
}

sequence().then(console.log) // finishes last => sequence is done: a b c
parallel().then(console.log) // finishes 2nd => parallel is done: a b c
race().then(console.log) // finishes first => race is done: a
```

---

## 138: Threads, Concurrency and Parallelism

**Parallel** means working on multiple tasks simultaneously, while **concurrent** may refer to working on multiple tasks, the CPU is actually switching back and forth between them.

---
