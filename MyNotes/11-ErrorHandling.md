# Section 11: Error Handling

## 146: Section Overview

---

## 147: Errors in JavaScript

```js
throw 'Error2';   // String type
throw 42;         // Number type
throw true;       // Boolean type
throw Error
throw new Error // will create an instance of an Error in JavaScript and stop the execution of your script.

const myError = new Error('oopsie')
myError.message // "oopsie"
myError.name // "Error"
myError.stack // "Error: oopsie at <anonymous>:1:17"

function a() {
  const b =  new Error('what?')
  return b
}

a().stack
/*
Error: what??
  at a (<anonymous>:2:12)
  at <anonymous>:1:1
*/

let error = new Error(message);
let error2 = new SyntaxError(message);
let error3 = new ReferenceError(message);
```

![image](images/1570558359538.png)

---

## 148: Try Catch

```js
function fail() {
  try {
    console.log('this works');
    throw new Error('oopsie');
  } catch(e) {
    console.log('error', e); // catch requires an error parameter
  } finally {
    console.log('still good');
    return 'returning from fail';
  }
  console.log('never going to get here'); // not reachable
}
fail();
```

---

## 149: Async Error Handling

```js
Promise.resolve('asyncfail')
  .then(response => {
    console.log(response)
    throw new Error('#1 fail')
  })
  .then(response => {
    console.log(response)
  })
  .catch(err => {
    console.error('error', err.message)
  })
  .then(response => {
    console.log('hi am I still needed?', response)
    return 'done'
  })
  .catch(err => {
    console.error(err)
    return 'failed'
  })
```

---

## 150: Async Error Handling 2

```js
(async function() {
  try {
    await Promise.reject('oopsie')
  } catch (err) {
    console.error(err)
  }

  console.log('This is still good!')
})()

/*
oopsie
This is still good!
=> Promise { <pending> }
*/
```

---

## 151: Exercise: Error Handling

```js
(function () {
  try {
    throw new Error();
  } catch (err) {
    var err = 5;
    var boo = 10;
    console.log(err);
  }
  //Guess what the output is here:
  console.log(err);
  console.log(boo);
})();

/*
5
undefined
10
*/
```

---

## 152: Extending Errors

```js
class authenticationError extends Error {
  constructor(message) {
    super(message)
    this.name = 'AuthenticationError'
    this.message = message
  }
}

throw new authenticationError('oopsie')
/*
AuthenticationError: oopsie
    at evalmachine.<anonymous>:9:7
*/

class PermissionError extends Error {
  constructor(message) {
    super(message)
    this.name = 'PermissionError'
    this.message = message
    this.favouriteSnack = 'grapes'
  }
}

const a = new PermissionError('oopsie')
console.log(a.favouriteSnack) // => 'grapes'

class DatabaseError extends Error {
  constructor(message) {
    super(message)
    this.name = 'DatabaseError'
    this.message = message
  }
}
```

---

## 153: Section Review

Done!!!

---
