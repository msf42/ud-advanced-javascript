# Section 7: Functional Programming

## 106: Section Overview

![image](images/1569958571167.png)

---

## 107: Functional Programming Introduction

- Functional Programming: Data and programming are kept separate.
- Generally don't have concepts of *Classes* and *Objects*
- Instead, functions operate on well-defined data structures, like arrays and objects, rather than *belonging* to that object.
- Goals are the same as in OOP, it is just done differently.
- All objects created in functional programming are *immutable*.
- FP has more rules and constraints than OOP.

---

## 108: Exercise: Amazon

```js
// Amazon shopping
const user = {
  name: 'Kim',
  active: true,
  cart: [],
  purchases: []
}


//Implement a cart feature:
// 1. Add items to cart.
// 2. Add 3% tax to item in cart
// 3. Buy item: cart --> purchases
// 4. Empty cart

//Bonus:
// accept refunds.
// Track user history.
```

---

## 109: Pure Functions

- A function must always return the same output when given the same input.
- It must not modify anything outside itself - no side effects.

### A function with side effects

```js
const array = [1,2,3]
function mutateArray(array) {
  arr.pop()
}
mutateArray(array); // undefined
console.log(array); // [1, 2]
mutateArray(array); // undefined
console.log(array); // [1]
```

---

## 110: Pure Functions 2

### Functions with no side effects

```js
const array = [1,2,3]
function removeLastItem(arr) {
  const newArray = [].concat(arr);
  newArray.pop();
  return newArray;
}

removeLastItem(array); // => [1, 2]
console.log(array); // => [1, 2, 3]

function multiplyBy2(arr) {
  return arr.map(item => item*2);
}

multiplyBy2(array); // => [ 2, 4, 6 ]
console.log(array); // => [1, 2, 3]
// map always returns a new array
```

Referential Transparency = if I change a reference to a function with its expected output, it won't change the program.

---

## 111: Can Everything Be Pure

No - we must interact with the DOM, the user, etc.

The goal of FP is not to write only pure functions, *but to minimize side effects.*

![image](images/1570110974839.png)

---

## 112: Idempotent

```js
function notGood(num) {
  return Math.random()
}

notGood(3);
// Idempotence: a function that always does what we expect it to do
```

---

## 113: Imperative vs Declarative

![1570111435367](images/1570111435367.png)

Declarative <<<----------------------------------------------------------->>>Imperative

```js
// imperative
for (let i = 0; i < 10; i++) {
  console.log(i);
}

// more declarative
[1,2,3,4,5].forEach(item => console.log(item))
```

- jQuery is much more imperative than React, Angular, or Vue
- Functional Programming helps us be more Declarative.

---

## 114: Immutability

```js
const obj = {name: 'Andrei'}

function clone(obj) {
  return {...obj}; // this is pure
}

obj.name = 'Nana'

obj; // => { name: 'Nana' }

// A better way; maintaining immutability
function updateName(obj) {
  const obj2 = clone(obj);
  obj2.name = 'Steve';
  return obj2
}

console.log(obj); // => { name: 'Nana' }
const updatedObj = updateName(obj);
console.log(obj); // => { name: 'Nana' }
console.log(updatedObj); // => { name: 'Steve' }
```

![image](images/1570113630203.png)

- **Structural Sharing** - When a function is copied, only the parts that are changed get a 'new' copy. This saves memory.

---

## 115: Higher Order Functions and Closures

### HOF

- Takes one or more functions as an argument OR returns another function (a callback)

```js
const hof = () => () => 5;
hof(); // => [Function]
hof()(); // => 5

const hof2 = (fn) => fn(5);
hof2(function a(x) {return x}); // => 5
```

## Closures

A mechanism for containing some sort of state.

```js
const closure = function() {
  let count = 0;
  return function increment() {
    count++;
    return count;
  }
}

closure() // => [Function: increment]

const incrementFN = closure();

incrementFN() // => 1
incrementFN() // => 2
incrementFN() // => 3

/* Since we assigned it to the const incrementFN, it remembers the value of count */
```

---

## 116: Currying

Currying is taking a single function with multiple arguments, and turning it into multiple functions with single arguments.

```js
const multiply = (a,b) => a*b;

multiply(3,4); // => 12

const curriedMultiply = (a) => (b) => a*b;
curriedMultiply(5,3); // => doesn't work
curriedMultiply(5)(3); // => 15

// Here is how it is useful:
const curriedMultiplyBy5 = curriedMultiply(5);
curriedMultiplyBy5(7); // => 35
```

---

## 117: Partial Application

A way to partially apply a function; producing a function with a smaller number of parameters.

```js
const multiply = (a,b,c) => a*b*c;

// through currying - we expect one argument at time
const curriedMultiply = (a) => (b) => (c) => a*b*c;
curriedMultiply(3)(4)(10) // => 120

// partial application - on the second call, we excpect the rest of the arguments
const partialMultiplyBy5 = multiply.bind(null, 5);
partialMultiplyBy5(3 , 4) // => 60
```

---

## 118 - 120: From Master the Coding Interview Course

---

## 121: Compose and Pipe

```js
// Compose - Any data transformation we do should be obvious
// data --> fn --> data --> fn -->

const compose = (f, g) => (data) => f(g(data))
const multiplyBy3 = (num) => num*3;
const makePositive = (num) => Math.abs(num);
const multiplyBy3AndAbsolute = compose(multiplyBy3, makePositive)

multiplyBy3AndAbsolute(-50);

// Pipe - similar, but goes left to right
// Instead of
const compose = (f, g) => (data) => f(g(data))
// We have
const pipe = (f, g) => (data) => g(f(data))

// These functions have same output
compose(fn1, fn2, fn3)(50)
pipe(fn3, fn2, fn1)(50)
```

---

## 122: Arity

**Arity** - the number of arguments a function takes; best to keep it lower

---

## 123: Is FP the Answer to Everything?

Maybe....

---

## 124: Solution: Amazon

```js
// Create user
const user = {
  name: 'Kim',
  active: true,
  cart: [],
  purchases: []
}
// Create array of their history
const history1 = [];

// pipe does the first function first
const pipe = (f, g) => (...args) => g(f(...args))
const purchaseItem2  = (...fns) => fns.reduce(pipe);
purchaseItem2(
  addItemToCart,
  applyTaxToItems,
  buyItem,
  emptyUserCart,
)(user, {name: 'laptop', price: 60})

// Not using this version
const compose = (f, g) => (...args) => f(g(...args))
const purchaseItem  = (...fns) => fns.reduce(compose);
// purchaseItem(
//   emptyUserCart,
//   buyItem,
//   applyTaxToItems,
//   addItemToCart
// )(user, {name: 'laptop', price: 50})

function addItemToCart(user, item) {
  history1.push(user)
  const updatedCart = user.cart.concat(item)
  return Object.assign({}, user, {cart: updatedCart});
}

function applyTaxToItems(user) {
  history1.push(user)
  const {cart} = user;
  const taxRate = 1.03;
  const updatedCart = cart.map(item => {
    return {
      name: item.name,
      price: item.price*taxRate
    }
  })
  return Object.assign({}, user, { cart: updatedCart });
}

function buyItem(user) {
  history1.push(user)
  const itemsInCart = user.cart;
  return Object.assign({}, user, { purchases: itemsInCart });
}
function emptyUserCart(user) {
  history1.push(user)
  return Object.assign({}, user, { cart: [] });
}

function refundItem() {}

function getUserState() {}

function goBack() {}

function goForward() {}
```

---

## 125: Reviewing FP

Review

---
