# Section 05: The Two Pillars: Closures and Prototypal Inheritance

## 63: Section Overview

---

## 64: Functions are Objects

### Four ways of calling functions

```js
// 1
> function one() {
... return 1
... }

> one()
1

// 2
> const obj = {
... two: function() {
..... return 2;
..... }
... }
undefined

> obj.two()
2

// 3
> function three() {
... return 3;
... }
undefined

> three.call()
3

// 4
> const four = new Function('return 4') // Function Constructor
undefined

> four()
4

> const five = new Function('num', 'return num')
undefined

> five(5)
5

> five(8)
8
```

### Functions are Objects

```js
> function woohooo() {
... console.log('woohooo')
... }
undefined

> woohooo.yell = 'ahhhhh'
'ahhhhh'

> woohooo
{ [Function: woohooo] yell: 'ahhhhh' }

> woohooo.name
'woohooo'
```

#### You get all of these automatically when you create a function

![image](images/1567184979413.png)

---

## 65: First Class Citizens

### Three Things

#### 1. Functions can be assigned to variables and properties

```js
var stuff = function(){}
```

#### 2. Arguments can be passed into a function

```js
> function a(fn) {
...     fn()
... }
undefined

> a(function() {console.log('hi there')})
hi there
```

#### 3. Functions can be returned as values to other functions

```js
> function b() {
... return function () {console.log('bye')}
... }
undefined

> b()
[Function]

> b()()
bye
undefined

> var d = b()
undefined

> d()
bye
undefined
```

---

## 66: Extra Bits: Functions

Be careful about calling functions within loops.

Defaults are helpful

```js
> function x(param=6) {
... return param;
... }
undefined

> x()
6

> x(4)
4
```

---

## 67: Higher Order Functions

- A function that returns another function, or a function that takes another function as an argument

### A simple function

```js
function letAdamLogin() {
  let array = [];
  for (let i = 0; i < 100000; i++) {
    array.push(i)
  }
  return 'Access Granted to Adam'
}

letAdamLogin()

// OUT: 'Access Granted to Adam'

function letEvaLogin() {
  let array = [];
  for (let i = 0; i < 100000; i++) {
    array.push(i)
  }
  return 'Access Granted to Eva'
}

letEvaLogin()

// OUT: 'Access Granted to Eva'
```

### A function that accepts parameters

```js
const giveAccessTo = (name) =>
  'Access granted to ' + name
  
function letUserLogin(user) {
  let array = [];
  for (let i = 0; i < 1000; i++) {
    array.push(i)
  }
  return giveAccessTo(user);
}

letUserLogin('Ned')

// OUT: 'Access granted to Ned'

function letAdminLogin(Admin) {
  let array = [];
  for (let i = 0; i < 100000; i++) {
    array.push(i)
  }
  return giveAccessTo(Admin);
}

letAdminLogin('steve')

//OUT: 'Access granted to steve'
```

### Higher Order Functions

```js
const giveAccessTo = (name) =>
  'Access granted to ' + name

function authenticate(verify) {
  let array = [];
  for (let i = 0; i < verify; i++) {
    array.push(i)
  }
  return true;
}

function letPerson(person, fn) {
  if (person.level === 'admin') {
    fn(500000)
  } else if (person.level === 'user') {
    fn(10000)
  }
  return giveAccessTo(person.name)
}

letPerson({level: 'user', name: 'Elvis'}, authenticate)

//OUT: 'Access granted to Elvis'

letPerson({level: 'admin', name: 'Jesus'}, authenticate)

//OUT: 'Access granted to Jesus'
```

---

## 68: Exercise: Higher Order Functions

```js
const multiplyBy = function(num1) {
  return function(num2) {
    return num1 * num2
  }
}

// Another way of writing the original, as an arrow function on a single line
const multiplyBy = (num1) => (num2) => num1*num2

const multiplyByTwo = multiplyBy(2);
multiplyByTwo(4);
// 8

multiplyByTwo(10);
// 20

const multiplyByFive = multiplyBy(5);
multiplyByFive(7);
// 35

multiplyBy(4)(6);
// 24
```

---

## 69: Closures

## The Two Pillars

![1567604211402](images/1567604211402.png)

### Closures: The combination of functions and the lexical environment

![1567604283712](images/1567604283712.png)

```js
function a() {
  let grandpa = 'grandpa'
  return function b() {
    let father = 'father'
    return function c() {
      let son = 'son'
      return '${grandpa} > ${father} > ${son}'
    }
  }
}

a()  // [Function: b]

a()()  // [Function: c]

a()()()  // '${grandpa} > ${father} > ${son}'
```

Note that `function c()` still has access to 'father' and 'grandpa'

![1567606776635](images/1567606776635.png)

Once we are done with `a()` or `b()`, they are discarded, but the variable remains, and is stored in the 'closure' box, since it is being referenced by another statement.

![1567606636273](images/1567606636273.png)

![1567606914030](images/1567606914030.png)

When the

```js
return '${grandpa} > ${father} > ${son}'
```

statement is called, JS looks in the global environment for 'grandpa' and 'father'. Since they aren't there, it looks in the closure environment.

In the example below, JS will garbage collect the `random` variable, since it is not referenced by a child function.

```js
function a() {
  let grandpa = 'grandpa'
  return function b() {
    let father = 'father'
    let random = 345673456
    return function c() {
      let son = 'son'
      return '${grandpa} > ${father} > ${son}'
    }
  }
}
```

![image](images/1567607274252.png)

JS scans the code lexically, and knows which variables to keep before the function is even called. It does this by storing them in the memory heap, rather than the call stack.

**Lexical Scope** - Where we **write** the code matters, not where we **call** it.

```js
function boo(string) {
  return function(name) {
    return function(name2) {
      console.log(`${string} ${name} ${name2}`)
    }
  }
}

// AS AN ARROW FUNCTION
const boo = (string)=>(name)=>(name2)=>
  console.log(`${string} ${name} ${name2}`)

boo('hi')
// [Function]

boo('hi')('tim')
// [Function]

boo('hi')('tim')('becca')
// hi tim becca

const booString = boo('hi')
/// You could wait years, then call:
const booStringName = booString()
```

---

## 70: Exercise: Closures

```js
function callMeMaybe() {
  const callMe = "Hi! I am now here!";
  setTimeout(function() {
    console.log(callMe);
  }, 4000);
}

callMeMaybe();
// ... after 4 seconds
// Hi! I am now here!


function callMeMaybe() {
  setTimeout(function() {
    console.log(callMe);
  }, 4000);
  const callMe = "Hi! I am now here!";
}

callMeMaybe();
// Works the same. JS sees that the enclosing function uses callMe, so it creates a closure
```

---

## 71: Closures and Memory

```js
function heavyDuty() {
  bigArray = new Array(7000).fill(':)')
  return bigArray
}

heavyDuty()
// returns 7000 :)s

////////////////////////////////////////////////
function heavyDuty(index) {
  const bigArray = new Array(7000).fill(':)')
  console.log('created!')
  return bigArray[index]
}

heavyDuty(588); heavyDuty(588); heavyDuty(588);
// created!
// created!
// created!
// ':)'

// What if this had to be run many times? The big array is getting created each time

function heavyDuty2() {
  const bigArray = new Array(7000).fill(':)')
  console.log('created again!')
  return function(index) {
    return bigArray[index]
  }
}

heavyDuty(588); heavyDuty(588); heavyDuty(588);
const getHeavyDuty = heavyDuty2();
getHeavyDuty(588)
getHeavyDuty(889)
getHeavyDuty(32)

// created!
// created!
// created!
// created again!
// ':)'
```

By using closures, the array only has to be created once, then it is stored in memory, rather than recreating it each time.

---

## 72: Closures and Encapsulation

```js
const makeNuclearButton = () => {
  let timeWithoutDestruction = 0;
  const passTime = () => timeWithoutDestruction++;
  const totalPeaceTime = () => timeWithoutDestruction;
  const launch = () => {
    timeWithoutDestruction = -1
    return 'boom';
  }
  setInterval(passTime, 1000)
  return {
    //launch: launch
    totalPeaceTime: totalPeaceTime
  }
}

const ohno = makeNuclearButton();

ohno.totalPeaceTime()
// 0

ohno.totalPeaceTime()
// 41

ohno.launch()
// 'boom'

// removed launch line //
ohno.launch()
// TypeError: ohno.launch is not a function
```

---

## 73 and 74: Exercise: Closures 2

### Initial function

Can be abused and run multiple times. How can we make it so that it can only be run once?

```js
let view;
function initialize() {
  view = 'mountain';
  console.log('view has been set!')
}

initialize();
initialize();
initialize();
console.log(view)
/*
view has been set!
view has been set!
view has been set!
mountain
*/
```

### Answer

```js
let view;
function initialize() {
  let called = 0;
  return function() {
    if (called > 0) {
      return;
    } else {
      view = 'mountain';
      called++;
      console.log('view has been set!')
    }
  }
}

const startOnce = initialize();
startOnce(); // view has been set!
startOnce(); // nothing
console.log(view) // mountain
```

---

## 75 and 76: Exercise: Closures 3

```js
const array = [1, 2, 3, 4];
for (let i = 0; i < array.length; i++) {
  setTimeout(function(){
    console.log('I am at index ' + i)
  }, 3000)
}
/*
I am at index 0
I am at index 1
I am at index 2
I am at index 3
*/
```

If we used *var* instead of *let*, it would have returned '4' on each line. This is because *let* creates a closure scope for `i`, so that it takes the value on each loop. With *var*, by the time the Timeout is finished, the loop has gone to 4.

### We can also get around it with an Immediately Invoked Function

```js
const array = [1, 2, 3, 4];
for (var i = 0; i < array.length; i++) {
  (function(closureI) {
    setTimeout(function(){
      console.log('I am at index ' + closureI)
    }, 3000)  
  })(i) // pass i to the IIF
}
/*
I am at index 0
I am at index 1
I am at index 2
I am at index 3
*/
```

---

## 77: Closures Review

A Closure is the product of the function and the lexical environment in which it is found.

---

## 78: Prototypal Inheritance

Inheritance: an object getting access to the properties and methods of another object

---

## 79: Prototypal Inheritance 2

```js
const array = [];
// undefined
array.__proto__
// [constructor: ƒ, concat: ƒ, copyWithin: ƒ, fill: ƒ, find: ƒ, …]
```

![image](images/1568745023210.png)

Prototype takes us **up** the chain, to the object.

```js
array.__proto__.__proto__;
/*
{constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}
constructor: ƒ Object()
hasOwnProperty: ƒ hasOwnProperty()
isPrototypeOf: ƒ isPrototypeOf()
propertyIsEnumerable: ƒ propertyIsEnumerable()
toLocaleString: ƒ toLocaleString()
toString: ƒ toString()
valueOf: ƒ valueOf()
__defineGetter__: ƒ __defineGetter__()
__defineSetter__: ƒ __defineSetter__()
__lookupGetter__: ƒ __lookupGetter__()
__lookupSetter__: ƒ __lookupSetter__()
get __proto__: ƒ __proto__()
set __proto__: ƒ __proto__()
*/
```

---

## 80: Prototypal Inheritance 3

```js
let dragon = {
  name: 'Tanya',
  fire: true,
  fight() {
    return 5
  },
  sing() {
    if (this.fire) {
      return `I am ${this.name}, the breather of fire`
    }
  }
}

let lizard = {
  name: 'Kiki',
  fight() {
    return 1
  }
}

lizard.__proto__ = dragon;

lizard.sing() // 'I am Kiki, the breather of fire'

lizard.fire // true

lizard.fight() // 1, because fight is already defined in the lizard object

dragon.isPrototypeOf(lizard) // true

lizard.__proto__ = dragon;
for (let prop in lizard) {
  console.log(prop)
}
/*
name
fight
fire
sing
*/


lizard.__proto__ = dragon;
for (let prop in lizard) {
  if (lizard.hasOwnProperty(prop)) {
    console.log(prop)
  }
}
// all of the other properties are inherited from up the prototype chain
/*
name
fight
*/
```

**Never use `.__proto__` in production code!**

- Undefined is returned when JS moves all the way up the prototype chain and doesn't find an answer.

---

## 81: Prototypal Inheritance 4

![image](images/1568837182813.png)

Remember this? The one below is more accurate.

![image](images/1568837290808.png)

```js
const obj = {name: 'Sally'};
obj.hasOwnProperty('name'); // true

function a() {};
a.hasOwnProperty('call'); // false
a.hasOwnProperty('bind'); // false
a.hasOwnProperty('apply'); // false
a.hasOwnProperty('name'); // true
```

![1568837699944](images/1568837699944.png)

```js
function multiplyBy5(num) {
  return num * 5
}

multiplyBy5.__proto__ // [Function]

temp1 = multiplyBy5;
temp1.name // 'multiplyBy5'
temp1.apply // [Function: apply]
temp1.bind  // [Function: bind]
temp1.call  // [Function: call]

Object.prototype.__proto__ // null
```

![1568838140904](images/1568838140904.png)

`__proto__` is a pointer for up the chain `prototype`

---

## 82: Prototypal Inheritance 5

### The *right* way to use prototypal inheritance (one of them, anyway)

```js
let human = {
  mortal: true
}

let socrates = Object.create(human);
console.log(socrates) //undefined

socrates.age = 45;
console.log(socrates) // { age: 45 }

console.log(socrates.mortal) // true

console.log(human.isPrototypeOf(socrates)) // true
```

---

## 83: Prototypal Inheritance 6

- Only functions have the prototype property

```js
function multiplyBy5(num) {
  return num*5
}

multiplyBy5.prototype // multiplyBy5 {}

multiplyBy5.__proto__ // [Function]

Function.prototype // [Function]

multiplyBy5.__proto__.__proto__ // {constructor}

Object.prototype // {constructor} // this is the base object

typeof Object // 'function'
// AND
typeof {} // 'function'
// HOWEVER...
typeof Object.prototype // 'object'

'string'.prototype // undefined

String.prototype // [String: '']
```

---

## 84 and 85: Exercise: Prototypal Inheritance

```js
//#1
//Date object => to have new method .lastYear() which shows you last year 'YYYY' format.

var Date = function(date) {
  this.lastYear = function() {
    return parseInt(date.substring(0,4)) - 1;
  }
}

new Date('1900-10-10').lastYear() //'1899'


//#Bonus
// Mofify .map() to print '🗺' at the end of each item.
function myFunction(num) {
  return num + "🗺";
}
console.log([1,2,3].map(myFunction))
//1🗺, 2🗺, 3🗺
```

### His solutions

```js
Date.prototype.lastYear = function() {
  return this.getFullYear() - 1;
}

Array.prototype.map = function() = {
  let arr = [];
  for ( let i = 0; i < this.length; i++) {
    arr.push((this[i] + "🗺"))
  }
  return arr;
}
```

---

## 86: Exercise: Prototypal Inheritance with `this`

How would you be able to create your own `.bind()` method using `call` or `apply`?

```js
Function.prototype.bind = function(whoIsCallingMe){
  const self = this; // <<<<====
  return function(){
    return self.apply(whoIsCallingMe, arguments);
  };
}
```

---

## 87: Section Review

### Scheme and Java

- These two languages were the inspiration for JS

---
