# Section 02: JavaScript Foundation

---

## 04: Section Overview

[Overview](https://coggle.it/diagram/XE3ZoVj-rtA5hcxj/t/advanced-javascript)

---

## 05: JavaScript Engine

[ECMAScript Engines](https://en.wikipedia.org/wiki/List_of_ECMAScript_engines)

---

## 06: Exercise: JavaScript Engine

Done

---

## 07: Inside the Engine

[astexplorer.net](https://astexplorer.net/)

---

## 08: Exercise: JS Engine for All

Done

---

## 09: Interpreters and Compilers

Interpreters - Interpret code line by line
Compilers - Translate the code into a lower language

---

## 10: Babel + TypeScript

- [Babel](https://babeljs.io/) is a Javascript compiler that takes your modern JS code and returns  browser compatible JS (older JS code).
- [Typescript](https://www.typescriptlang.org/) is a superset of Javascript that compiles down to Javascript.

Both of these do exactly what compilers do: Take one language and convert into a different one!

---

## 11: Inside the V8 Engine

- Compilers take longer to get up and running, but they optimize the code, which Interpreters do not.
- JIT Compiler (Just In Time Compiler)
- Profiler - monitors code as it runs; optimizes it

---

## 12: Comparing Other Languages

Done

---

## 13: Writing Optimized Code

[Hidden Classes](https://richardartoul.github.io/jekyll/update/2015/04/26/hidden-classes.html)

[Managing Arguments](https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#3-managing-arguments)

### Careful with these

- eval()
- arguments
- for in loops
  - use object.keys and iterate
- with
- delete

### Inline Caching

- Code that gets run repeatedly run is replaced by shortcut

### Hidden Classes

- Assign all properties of an object in its constructor, or add things in the same order

---

## 14: WebAssembly

[Web Assembly](https://webassembly.org/)

---

## 15: Call Stack and Memory Heap

|              **Call Stack**               |                  **Memory Heap**                   |
| :------------------: | :--------------------: |
| A place to store variables, objects, data | A place to run and keep track of what is happening |
|  Keeps track of where we are in the code  |          Where memory allocation happens           |
|  Operates on a First In, Last Out basis   |                                                    |

### Memory Heap

```js
// allocate memory for a number
const number = 610;  
// allocate memory for a string
const string = 'some text';
// allocate memory for an object and its values
const human = {
  first: 'Steve',
  last: 'Furches'
};
```

### Call Stack

```js
function subtractTwo(num) {
  return num - 2;
}
function calculate() {
  const sumTotal = 4 + 5;
  return subtractTwo(sumTotal);
}
calculate() // returns 7
```

- Calculate is the first into the stack, and the last out.

---

## 16: Stack Overflow

- Recursion - a function calling itself.
  - Can actually be useful in special cases

---

## 17: Garbage Collection

- JavaScript is a garbage collected language
  - But, we still have to consider memory management
  
---

## 18: Memory Leaks

[Garbage Collection in Redux Applications](https://developers.soundcloud.com/blog/garbage-collection-in-redux-applications)

[WindowOrWorkerGlobalScope.setInterval()](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/setInterval)

### Example

```js
let array = [];
for (let i = 5; i > 1; i++) {
  array.push(i-1)
}
```

### Global Variables

```js
var a = 1;
var b = 2;
var c = 3;
// these are all global and use up memory, and can't be used elsewhere
```

### Event Listeners

```js
var element = document.geteElementById('button');
element.addEventListener('click', onClick);
// when a lot of these are added without getting removed, they take up memory
```

### `setInterval`

```js
setInterval(() => {
  // referencing objects
})
```

---

## 19: Single Threaded

- A single call stack. Functions are not run in parallel.
- JS is synchronous - everything in order, one at a time.

---

## 20: Exercise: Issue with Single Thread

![image](images/s_B54682212FFE635E68DEFFC20BF5731476C4FC913C818608A659EC6FA22BC755_1565095692482_image.png)

- There is more than just the JS engine, call stack, and memory heap.

---

## 21: JavaScript Runtime

[http://latentflip.com/loupe](https://bit.ly/2FUkE0G)

![image](images/s_B54682212FFE635E68DEFFC20BF5731476C4FC913C818608A659EC6FA22BC755_1565095692482_image-1565979416500.png)

### Web API

- send http requests
- listen to DOM events
- caching and database managment
- Asynchronous
- Browsers use low level programming languages to perform things in the background
- When Web API performs a function, it sends the info to the Callback Queue. When the call stack is available, the event loop will check the Callback Queue and send information to it.

- Event Loop - “Is the Call Stack empty?… Is the Call Stack empty?”

```js
console.log('1');
console.log('3'); // Runs before Web API calls back with '2'
setTimeout(() => {console.log('2')}, 1000); // sent to Web API
// returns 1 3 2
```

- Using this method gives us the power of asynchronous code.

---

## 22: Node.js

[node.js](https://nodejs.org/en/)

- Allows JS to be run outside the browser.
  - The only way to run it until 2009.
- Written in C++

---

## 23: Exercise: Fix This Code

### Broken

```js
//fill array with 60000 elements
const list = new Array(60000).join('1.1').split('.');

function removeItemsFromList() {
  var item = list.pop();

  if (item) {
      removeItemsFromList();
  }
};

removeItemsFromList();
```

### Fixed

```js
const list = new Array(60000).join('1.1').split('.');

function removeItemsFromList() {
    var item = list.pop();

    if (item) {
      setTimeout(removeItemsFromList, 0);
    }
};
removeItemsFromList();
list
```

---

## 24: Section Review

[JS Runtime](https://repl.it/@aneagoie/Javascript-Runtime)

---
