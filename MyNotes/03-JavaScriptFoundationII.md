# Section 03: JavaScript Foundation II

---

## 25: Section Overview

Just an overview

---

## 26: Execution Context

### Example

```js
function printName() {
  return 'Steve Furches'
}

function findName() {
  return printName()
}

function sayMyName() {
  return findName()
}

sayMyName()
```

### Execution Context

![image](images/s_5018E4C089EA65053E93EA1F7833154EB9135D956CFDBB9A1BC79F0327BBEDE4_1565106156264_image.png)

`global()` gets popped off after the final line of code is executed.

![image](images/s_5018E4C089EA65053E93EA1F7833154EB9135D956CFDBB9A1BC79F0327BBEDE4_1565106481339_image.png)

---

## 27: Lexical Environment

- What ‘universe’ is the function part of?
  - Global? Function x? Function y?
- Every time we create a function, we create a new ‘world’ inside that function
- Execution Context tells you which Lexical Environment you are working in

**In JavaScript, our lexical scope (available data + variables where the function is defined) determines our available variables - NOT where the function is called (dynamic scope*)**

---

## 28: Hoisting

![image](images/s_5018E4C089EA65053E93EA1F7833154EB9135D956CFDBB9A1BC79F0327BBEDE4_1565110239349_image.png)

```js
console.log('1-------')
console.log(teddy) // returns undefined
console.log(sing()) // returns ohh la la
// ^^this is hoisting^^
// JS temporarily sets this as 'undefined' until it gets defined
// variables are hoisted, but not their value - but NOT let or const, only var
// functions are fully hoisted

var teddy = 'bear';

function sing() {
  console.log('ohh la la')
}

// writing in () means it won't be hoisted
(function sing() {
  console.log('ohh la la')
})
// this is a function expression. It will not be hoisted
var sing2 = function() {
  console.log('uhh la la')
}

// this is a function declaration
function sing() {
  console.log('ohh la la')
}
```

---

## 29: Exercise: Hoisting

```js
var one = 1;
var one = 2;
console.log(one) // returns 2
```

```js
a(); // returns bye, no matter where we put a()
function a() {
  console.log('hi')
}
function a() {
  console.log('bye')
}
```

---

## 30: Exercise: Hoisting 2

```js
var favoriteFood = "grapes"; // gets hoisted as undefined

// this gets hoisted as undefined
var foodThoughts = function () {
  console.log("Original favorite food: " + favoriteFood);
  var favoriteFood = "sushi";
  console.log("New favorite food: " + favoriteFood);
};
foodThoughts()
// when the function is run, a new execution phase is created. var favoriteFood is hoisted as undefined

// Output
// Original favorite food: undefined
// New favorite food: sushi

// If we eliminate the 'sushi' line, the results are:
// Original favorite food: grapes
// New favorite food: grapes
```

---

## 31: Exercise: Hoisting 3

```js
function bigBrother(){
  function littleBrother() {
    return 'it is me!';
  }
  return littleBrother();
  function littleBrother() {
    return 'no me!';
  }
}
// Got this one correct. Returns 'no me!'
bigBrother();
```

---

## 32: Function Invocation

```js
// Function Expression (not hoisted); defined at runtime
var canada = function() {
  console.log('cold')
}
// Function Declaration (hoisted); defined at parsetime
function india() {
  console.log('warm')
}
// Function Invocation/Call/Execution
canada()
india()
function marry(person1, person2) {
  console.log(arguments)
  return `${person1} is now married to ${person2}`
}
marry('Steve', 'Annie')
// returns
// [Arguments] { '0': 'Steve', '1': 'Annie' }
// => 'Steve is now married to Annie'
```

---

## 33: arguments Keyword

```js
// Function Expression (not hoisted); defined at runtime
var canada = function() {
  console.log('cold')
}

// Function Declaration (hoisted); defined at parsetime
function india() {
  console.log('warm')
}

// Function Invocation/Call/Execution
canada()
india()
function marry(person1, person2) {
  console.log(arguments)
  return `${person1} is now married to ${person2}`
}
marry('Steve', 'Annie')
// returns
// [Arguments] { '0': 'Steve', '1': 'Annie' }
// => 'Steve is now married to Annie'
// arguments looks like an array, but it isn't
// you can't use array methods on it
// instead, use
console.log(Array.from(arguments))

// Rest Parameters
function marry2(...args) {
  console.log(args)
  console.log(Array.from(arguments))
  return `${args[0]} is now married to ${args[1]}`
}
marry2('Steve', 'Annie')
```

---

## 34: Variable Environment

![image](images/s_5018E4C089EA65053E93EA1F7833154EB9135D956CFDBB9A1BC79F0327BBEDE4_1565125830848_image.png)

```js
function two() {
  var isValid;
}
function one() {
  var isValid = true;
  two();
}
var isValid = false;
one();
// since two() is the last thing run, isValid is undefined
// from the bottom up
// two() -- undefined
// one() -- true
// global() -- false
```

---

## 35: Scope Chain

- Each function has a connection to its ‘outside’ world, depending on its lexical environment.
  
```js
function printName() {
  return 'Steve Furches'
}

function findName() {
  return printName()
}

function sayMyName() {
  return findName()
}
sayMyName()
```

![image](images/s_5018E4C089EA65053E93EA1F7833154EB9135D956CFDBB9A1BC79F0327BBEDE4_1565181278061_image.png)

- Each function has its own variable environment, but each is connected by the scope chain to the global variable environment.
  - The scope chain starts where the variable is defined and goes all the way down to the global context to see if the variable exists.

```js
function sayMyName() {
  var a = 'a';
  return function findName() {
    var b = 'b';
    return function printName() {
      var c = 'c';
      return 'Steve Furches'
    }
  }
}
sayMyName() // returns nothing [function sayMyName]
sayMyName()() // returns nothing [function findName]
sayMyName()()() // returns 'Steve Furches'
```

![image](images/s_5018E4C089EA65053E93EA1F7833154EB9135D956CFDBB9A1BC79F0327BBEDE4_1565184973180_image.png)

```js
function sayMyName() {
  var a = 'a';
  return function findName() {
    var b = 'b';
    console.log(a) // works
    console.log(c) // ERROR - see image above
    return function printName() {
      var c = 'c';
      console.log(a) // works
      console.log(b) // works
      console.log(c) // works
      console.log('Steve Furches')
    }
  }
}
sayMyName() // returns nothing [function sayMyName]
sayMyName()() // returns nothing [function findName]
sayMyName()()() // returns 'Steve Furches'
```

---

## 36: [[scope]]

![image](images/s_5018E4C089EA65053E93EA1F7833154EB9135D956CFDBB9A1BC79F0327BBEDE4_1565185832141_image.png)

Note the [[Scopes]] for a() - global

---

## 37: Exercise: JS is Weird

### Global leakage

- since there is no ‘var’ or ‘let’ in front of height, it gets bumped up to the global lexical environment.
  - `use strict` prevents this

```js
function weird() {
height = 50;
console.log(height)
}
weird() // returns 50
console.log(height) // returns 50 as well!
```

```js
var heyhey = function doodle() {
  console.log('heyhey')
}
heyhey() // returns 'heyhey'
doodle() // returns reference error
// doodle has its own lexical environment
```

---

## 38: Function Scope vs Block Scope

```js
// function scope (var)
// vs
// block scope (let and const)
if (5 > 4) {
  var secret = '12345';
}
console.log(secret); //returns 12345
// because secret is NOT functionally scoped
function a() {
  var secret = 12345;
}
console.log(secret); // REF ERROR
// because it IS functionally scoped
// However, we can can use Block Scoping with the let and const keywords
if (5 > 4) {
  let secret = '12345';
}
console.log(secret); // returns an error
```

---

## 39: Exercise: Block Scope

```js
function loop() {
  for (var i = 0; i < 5; i++) {
    console.log(i)
  }
  console.log('final', i) // would produce error if 'let' instead of 'var'
}
loop()

// OUTPUT
0
1
2
3
4
final 5
```

---

## 40: Global Variables

- Polluting the Global Namespace: Memory is limited. Global variables take up space and stay there.
- Variable Collisions: Global variables from different JS files override each other if there are duplicates, leading to bugs.

---

## 41: IIFE

### IIFE (“Iffy”) = Immediately Invoked Functional Expression

```js
(function() {
  // do this
})();
```

- Wrapping it in () make it a functional expression rather than a functional declaration.
  - This keeps variables local
- The last () will immediately invoke it.
  - You can’t do this with a function declaration.

![image](images/s_5018E4C089EA65053E93EA1F7833154EB9135D956CFDBB9A1BC79F0327BBEDE4_1565191207823_image.png)

---

## 42: this Keyword

`this` is the object that the function is a property of

```js
function a() {
  console.log(this)
}

a() // returns Window
```

```js
function b() {
    'use strict'
    console.log(this)
}

b() // returns undefined
```

```js
const obj = {
  name: 'Billy',
  sing: function() {
    return 'lalala ' + this.name;
  }
}

console.log(obj.sing()) // returns lalala Billy

// a newer syntax:
const obj = {
  name: 'Billy',
  sing() {
    return 'lalala ' + this.name;
  }
}
```

### `this` refers to whatever is left of the dot

### What is the point of the `this` keyword?

1. gives methods access to their object
2. execute same code for multiple objects

```js
function importantPerson() {
  console.log(this.name)
}

const name = 'Sunny';
const obj1 = {
  name: 'Cassy',
  importantPerson: importantPerson
}

const obj2 = {
  name: 'Jacob',
  importantPerson: importantPerson
}

obj1.importantPerson() // returns Cassy
console.log(name) // returns Sunny
```

---

## 43: Exercise: Dynamic Scope vs Lexical Scope

```js
const a = function() {
  console.log('a', this)
  const b = function() {
    console.log('b', this)
    const c = {
      hi: function() {
        console.log('c', this) // this is the function hi
      }
    }
    c.hi()
  }
  b()
}

a()

// result
// a > window, because we are calling window.a
// b > window, because we are calling window.a(b)
// c > {hi: [Function: hi]}, because c is an object (hi), being called by the function
```

```js
const obj = {
  name: 'Billy',
  sing() {
    console.log('a', this);
    var anotherFunction = function() {
      console.log('b', this)
    }
    anotherFunction()
  }
}

obj.sing()
// output
// a {name: "Billy", sing: f}
// b window
```

### In JavaScript our __lexical scope__ (available data + variables where the function was defined) determines our available variables - NOT where the function is called (dynamic scope)

- Everything in JavaScript is lexically scoped, except for the `this` keyword, which is dynamically scoped

### Using Arrow Functions

- However, **arrow functions** are lexical. Changing this to an arrow function gives the following results:

```js
const obj = {
  name: 'Billy',
  sing() {
    console.log('a', this);
    var anotherFunction = () => {
      console.log('b', this)
    }
    anotherFunction()
  }
}

obj.sing()
// output
// a {name: "Billy", sing: f}
// b {name: "Billy", sing: f}
```

### Another way

```js
const obj = {
  name: 'Billy',
  sing() {
    console.log('a', this);
    var anotherFunction = function() {
      console.log('b', this)
    }
    return anotherFunction.bind(this)
  }
}

obj.sing()
// output
// a { name: 'Billy', sing: [Function: sing] }

obj.sing()()
// output
// a { name: 'Billy', sing: [Function: sing] }
// b { name: 'Billy', sing: [Function: sing] }
```

### One Last Way

```js
const obj = {
  name: 'Billy',
  sing() {
    console.log('a', this);
    var self = this; // locks down 'self' for current value of 'this'
    var anotherFunction = function() {
      console.log('b', self)
    }
    anotherFunction()
  }
}

obj.sing()
// output
// a { name: 'Billy', sing: [Function: sing] }
// b { name: 'Billy', sing: [Function: sing] }
```

---

## 44: call(), apply(), bind()

```js
function a() {
  console.log('hi')
}

a.call() // returns 'hi'
a() // just short for a.call()
```

```js
const wizard = {
  name: 'Merlin',
  health: 90,
  heal() {
    return this.health = 100;
  }
}

const archer = {
  name: 'Robin Hood',
  health: 30
}

console.log(wizard.health) // 90
console.log(wizard.heal()) // 100

console.log(archer.health) // 30
console.log(wizard.heal.call(archer)) // returns 100

const healArcher = wizard.heal.bind(archer)
healArcher()
console.log(archer) // returns { name: 'Robin Hood', health: 100 }
```

### `.apply()` works the same as call, except it takes an array, rather than comma separated values as in `.call()`

### `.bind()` stores a function for later use

---

## 45: Exercise: call(), apply()

### His solution:

```js
const array = [1,2,3];

function getMaxNumber(arr){
    return Math.max.apply(null, arr);
}

console.log(getMaxNumber(array)) // should return 3
```

### Here is my solution:

```js
const array = [1,2,3];

function getMaxNumber(arr){
    return Math.max(...arr);
}

console.log(getMaxNumber(array))
```

---

## 46: bind() and currying

```js
function multiply(a, b) {
  return a*b
}

let multiplyByTwo = multiply.bind(this, 2)

console.log(multiplyByTwo) // [Function: bound multiply]

console.log(multiplyByTwo(4)) // 8
```

---

## 47: Exercise: this Keyword

```js
var b = {
  name: 'jay',
  say() {console.log(this)}
}

var c = {
  name: 'jay',
  say() {return function() {console.log(this)}}
}

var d = {
  name: 'jay',
  say() {return () => console.log(this)}
}

console.log(b.say())  // {name: "jay", say: ƒ}
console.log(c.say()) //  ƒ () {console.log(this)}
console.log(c.say()()) // window
console.log(d.say()) //  () => console.log(this)
console.log(d.say()()) // {name: "jay", say: ƒ}
```

---

## 48: Exercise: this Keyword 2

```js
const character = {
  name: 'Simon',
  getCharacter() {
    return this.name;
  }
};
// const giveMeTheCharacterNOW = character.getCharacter;
const giveMeTheCharacterNOW = character.getCharacter.bind(character);

console.log('?', giveMeTheCharacterNOW());
```

---

## 49: Context vs Scope

Scope - what is the variable environment

Context - what is the value of `this` keyword

---

## 50: Section Review

Done

---
