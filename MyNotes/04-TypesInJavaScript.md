# Section 04: Types in JavaScript

---

## 51: Section Overview

Done

---

## 52: JavaScript Types

```js
5 // numbers
true // boolean
'Hello World' // string
undefined
null
Symbol('just me') // Symbols, added in ES6
{} // objects
```

![1566501581711](images/1566501581711.png)

```js
// Primitive Types
typeof 5 // number
typeof true // boolean
typeof 'hello' // string
typeof undefined // undefined
typeof null // object; should be null
typeof Symbol('just me') // symbol

// Non-Primitive Types
typeof {} // object
typeof [] // object
typeof function() {} // function; really an object
```

---

## 53: Array.isArray()

```js
var array1 = [1, 2, 3];

var array2 = {
  0: 1,
  1: 2,
  2: 3
}

Array.isArray(array1) // true

Array.isArray(array2) // false
```

---

## 54: Pass By Value vs Pass By Reference

![1567009848349](images/1567009848349.png)

### Pass By Value (Primitive Types)

```js
> var a = 5;
undefined

> var b = a
undefined

> b++
5

> a
5

> b
6
```

### Pass By Reference (Objects)

```js
> let obj1 = {name: 'Yao', password: '123'};
undefined

> let obj2 = obj1;
undefined

> obj2.password = 'abc';
'abc'

> obj1
{ name: 'Yao', password: 'abc' }

> obj2
{ name: 'Yao', password: 'abc' }
```

```js
> var c = [1, 2, 3, 4, 5]
undefined

> var d = c;
undefined

> d.push(3245);
6

> d
[ 1, 2, 3, 4, 5, 3245 ]

> c
[ 1, 2, 3, 4, 5, 3245 ]

// by using [].concat(), we make a copy of the array
> var e = [].concat(c);
undefined

> e.push(43);
7

> e
[ 1, 2, 3, 4, 5, 3245, 43 ]

> d
[ 1, 2, 3, 4, 5, 3245 ]

> c
[ 1, 2, 3, 4, 5, 3245 ]
```

### Another Method

```js
> let obj = {a: 'a', b: 'b', c: 'c'}
undefined

> let clone = Object.assign({}, obj)
undefined

> obj.c = 5;
5

> clone
{ a: 'a', b: 'b', c: 'c' }

> obj
{ a: 'a', b: 'b', c: 5 }

> let clone2 = {...obj}
undefined

> obj.b = 4;
4

> clone2
{ a: 'a', b: 'b', c: 5 }

> clone
{ a: 'a', b: 'b', c: 'c' }

> obj
{ a: 'a', b: 4, c: 5 }
```

### Shallow Clone vs Deep Clone

```js
> let newObj = {
... a: 'a',
... b: 'b',
... c: {
..... deep: 'try to copy me'
..... }
... };
undefined

> let newClone = Object.assign({}, newObj);
undefined

> let newClone2 = {...newObj}
undefined

> newObj.c.deep = "haha";
'haha'

> newObj
{ a: 'a', b: 'b', c: { deep: 'haha' } }

> newClone
{ a: 'a', b: 'b', c: { deep: 'haha' } }

> newClone2
{ a: 'a', b: 'b', c: { deep: 'haha' } }

> // this is a 'shallow clone'
undefined

> let superClone = JSON.parse(JSON.stringify(newObj))
undefined

> newObj.c.deep = "hello";
'hello'

> newObj
{ a: 'a', b: 'b', c: { deep: 'hello' } }

> superClone
{ a: 'a', b: 'b', c: { deep: 'haha' } }
```

---

## 55: Exercise: Compare Objects

```js
> var user1 = {name: "nerd", org: "dev"};
undefined

> var user2 = {name: "nerd", org: "dev"};
undefined

> user1 == user2
false

> JSON.stringify(user1) == JSON.stringify(user2)
true
```

---

## 56: Exercise: Pass By Reference

```js
const number = 100
const string = "Jay"
let obj1 = {
  value: "a"
}
let obj2 = {
  value: "b"
}
let obj3 = obj2;

function change(number, string, obj1, obj2) {
    number = number * 10;
    string = "Pete";
    obj1 = obj2;
    obj2.value = "c";
}

change(number, string, obj1, obj2);

//Guess the outputs here before you run the code:
console.log(number); // 100
console.log(string); // Jay
console.log(obj1.value); // a
console.log(obj2.value); // c
```

---

## 57: Type Coercion

```js
> 1 == '1'
true

> 1 === '1'
false

> if (5) {
... console.log('hello')
... }
hello

> if (0) {
... console.log('world')
... }

```

---

## 58: Exercise: Type Coercion

```js
> false == ""
true

> false == []
true

> false == {}
false

> "" == 0
true

> "" == []
true

> "" == {}
false

> 0 == []
true

> 0 == {}
false

> 0 == null
false
```

---

## 59: Quick Note: Upcoming Videos

60 - 62 are from the Junior to Senior course

---

## 60: JTS: Dynamic vs Static Typing

![1567180458098](images/1567180458098.png)

### Dynamically Typed (JavaScript)

```js
var a = 100;
```

### Statically Typed (C++)

- Variables must be declared explicitly, and are bound to a particular type

```c++
int a;
a = 100
```

#### Typescript is Statically Typed JavaScript

```typescript
function sum(a: number, b: number) {
    return a+b;
}

sum('hello', null); // this would get caught and not make it through
```

[TypeScript on Wikipedia](https://en.wikipedia.org/wiki/TypeScript)

*With Dynamically Typed languages, you spend more time on debugging logical errors, and less time debugging semantic errors.*

---

## 61: JTS: Weakly vs Strongly Typed

### JavaScript is Weakly Typed - It allows type coercion

```js
> var a = "Skippy"
undefined

> a + 17
'Skippy17'
```

### Python is Strongly Typed

```python
> a = "Skippy"
> a + 17
ERROR
```

---

## 62: JTS Static Typing in JavaScript

**Flow** adds types to JavaScript by passing its code through Babel (to remove non-JS code). The top line will have `// @flow` to indicate it is a Flow file.

**TypeScript** uses its own compiler. It is a *superset* of JS. It adds its own functionality on top of JS.

- Angular is built with TypeScript
- React community is also starting to use it, even though Flow was specifically developed to work with React.

**ReasonML** also has its own compiler, but it is a completely separate language.

**Elm** is similar to ReasonML.
