# Section 10: Modules in JavaScript

## 139: Section Overview

---

## 140: What is a Module?

### Contents of script.js

```js
var harry = 'potter'
var voldemort = 'He who must not be named'
function fight(char1, char2) {
  var attack1 = Math.floor(Math.random() * char1.length);
  var attack2 = Math.floor(Math.random() * char2.length);
  return attack1 > attack2 ? `${char1} wins` : `${char2} wins`
}

fight(harry, voldemort)
```

### HTML

```html
<script> src ='./script.js'</script>
<script> var fight = 'haha'</script>
```

- The second `fight` would overwrite the first. We can avoid this with modules. (next lesson)

---

## 141: Module Pattern

- Global Scope
  - Module Scope
    - Function Scope
      - Block Scope - `let` and `const`

Using Closure and Encapsulation to Create Module Scope

### IIFEs

```js
(function() {
  // global variables are available
  var harry = 'potter'
  var voldemort = 'He who must not be named'
  function fight(char1, char2) {
    var attack1 = Math.floor(Math.random() * char1.length);
    var attack2 = Math.floor(Math.random() * char2.length);
    return attack1 > attack2 ? `${char1} wins` : `${char2} wins`
  }
  console.log(fight(harry, voldemort))
})()
```

but `fight` is not available in the global namespace, unless...

### Module Pattern

```js
var fightModule = (function() {
  // global variables are available
  var harry = 'potter'
  var voldemort = 'He who must not be named'
  function fight(char1, char2) {
    var attack1 = Math.floor(Math.random() * char1.length);
    var attack2 = Math.floor(Math.random() * char2.length);
    return attack1 > attack2 ? `${char1} wins` : `${char2} wins`
  }
  return {
    fight: fight
  }
})()

fightModule // => { fight: [Function: fight] }

fightModule.fight('ron', 'hagrid') // => 'hagrid wins'
```

- If we call `fightModule.fight`, we still have access to everything in the module
- the `return` statement is key (the 'reveal') - it tells JS what we want to be available to the outside world

---

## 142: Module Pattern Pros and Cons

- Pros
  - Outlined above

- Cons
  - Technically still polluting the global namespace
  - We don't know the dependencies

---

## 143: CommonJS, AMD, UMD

Not much to take notes on.... A bunch of confusing ways to get around dependency resolution and pollution of the global namespace. On to ES6 Modules!

---

## 144: ES6 Modules

### script.js

```js
const harry = 'potter'
const voldemort = 'He who must not be named'

function jump() {
  //
}

export function fight(char1, char2) {
  const attack1 = Math.floor(Math.random() * char1.length);
  const attack2 = Math.floor(Math.random() * char2.length);
  return attack1 > attack2 ? `${char1} wins` : `${char2} wins`
}
```

### HTML

```html
<script type="module" src='./script.js'></script>
......
.......
<script type="module">
  import { fight } from 'script';
  console.log(fight('ron', 'hedwig'))
</script>
```

---

## 145: Section Review

Done.

---
