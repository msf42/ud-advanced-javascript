# Section 15: Intermediate JavaScript

## 200: Scope

- When working in a browser, you are working in the root (parent) scope.
- Functions have access to any variables in the root level.

### script.js

```js
//scope
// Root Scope (window)
var fun = 5;
function funFunction() {
  // child scope
  var fun = "hellooo";
  console.log(1, fun);
}
function funnerFunction() {
  // child scope
  var fun = "Byee";
  console.log(2, fun);
}
function funnestFunction() {
  // child scope
  fun = "AHHHH";
  console.log(3, fun);
}
console.log("window", fun); // returns window 5
funFunction();  // returns 1 fun
funnerFunction();  // returns 2 Byeee
funnestFunction();  // returns 3 AHHHH
```

---

## 201: Advanced Control Flow

### Ternary Operator

**Syntax:** `Condition? expr1 : expr2`

- Great for checking a condition, and providing two options. Can often substitute for if/else statements.

```js
// condition ? expr1 : expr2
function isUserValid(bool) {
  return bool;
}
var answer = isUserValid(true) ? "You may enter" : "Access denied"
var automateAnswer =
  "Your account # is " + ( isUserValid(false) ? "1234" : "not available")
```

### Switch

Switches are great substitutes for `if/elif/elif/elif/else`statements

```js
function moveCommand(direction) {
  var whatHappens;
  switch (direction) { // check direction
    case "forward": // is it forward?
      whatHappens = "you encounter a monster"
      break;  // do above, then leave the switch
    case "back": // is it back?
      whatHappens = "you arrive home"
      break;
    case "right":  // is it right?
      whatHappens = "you found a river"
      break;
    case "left":  // is it left?
      whatHappens = "you run into a troll"
      break;
    default:  // if none of those, then do this
      whatHappens = "please eenter a valid direction"
  }
  return whatHappens;
}
```

---

## 202: ES5 and ES6

ECMA International
ECMAscript == Javascript
ECMAscript Version 6 = ES6

[https://babeljs.io](https://babeljs.io)
BABEL allows you to write something in a newer version of JS, and it will convert it to an older version that ALL browsers know.

### Let & Const

```js
const player = 'bobby';
let experience = 100;
var wizardLevel = false;
if (experience > 90) {
  var wizardLevel = true; // var changes everywhere
  console.log('inside', wizardLevel); // true
}
console.log('outside', wizardLevel); // true
```

- Above returns inside true, outside true
- Below returns inside true, outside false

```js
const player = 'bobby';
let experience = 100;
let wizardLevel = false;
if (experience > 90) {
  let wizardLevel = true;
  console.log('inside', wizardLevel); // true
}
console.log('outside', wizardLevel); // false
```

If `let` is within `{}`, it creates a new *scope*. `var` does not.

**Do not use** `**var**` **anymore! Only use** `**let**`!

**Constants can not be changed.** You may even want to make a function a constant:

```js
const a = function() {
  console.log('a')
}
```

So, use `const` for items that can’t be changed, use `let` for items that can.

The `obj` constant can not be reassigned, but a property of it can.

```js
const obj = {
  player: 'bobby',
  experience: 100,
  wizardLevel: false
}

obj = 5 // can't reassign
VM252:1 Uncaught TypeError: Assignment to constant variable.
  at <anonymous>:1:5
(anonymous) @ VM252:1

obj.experience = 99
99
```

### Destructuring

```js
const obj = {
  player: 'bobby',
  experience: 100,
  wizardLevel: false
}

// before, you would have to do this to access these properties
const player = obj.player;
const experience = obj.experience;
let wizardLevel = obj.wizardLevel;

// now we can do this
const { player, experience } = obj;
let { wizardLevel } = obj;
```

```js
const name = 'john snow';
const obj = {
  [name]: 'hello',
  [1 + 2]: 'hihi'
}

obj // results
{3: "hihi", john snow: "hello"}
```

### Const properties

```js
const a = "Simon";
const b = true;
const c = {};

// instead of
const obj = {
  a: a,
  b: b,
  c: c
}

// do this if property and value are same
const obj = {
  a,
  b,
  c
}
```

### Template Strings

```js
const name = "sally";
const age = 34;
const pet = "horse";

// old way
// const greeting = "Hello " + name + "you\'re ....."

const greetingBest = `Hello ${name} you seem to be ${age - 10}. What a lovely ${pet} you have.`
undefined
greetingBest
"Hello sally you seem to be 24. What a lovely horse you have."
```

### Default Arguments

```js
function greet(name='', age=30, pet='cat') {
  return `Hello ${name} you seem to be ${age - 10}. What a lovely ${pet} you have.`
}
undefined
greet()
"Hello  you seem to be 20. What a lovely cat you have."

greet('steve', 43, 'nose')
"Hello steve you seem to be 33. What a lovely nose you have."
```

### Symbol

```js
let sym1 = Symbol();
let sym2 = Symbol('foo');
let sym3 = Symbol('foo');
undefined
sym1
Symbol()
sym2
Symbol(foo)
sym3
Symbol(foo)
sym2 == sym3  // because symbols are all unique
false
```

### Arrow Functions

```js
function add(a, b) {
  return a + b;
}

// Instead, use this
const add = (a,b) => a + b;

// Could still do a multi-line arrow function
const add = (a,b) => {
  return a + b;
}
```

---

### 203: Advanced Functions

```js
const first = () => {
  const greet = 'Hi';
  const second = () => {
    alert(greet);
  }
  return second;
}
const newFunc = first();
newFunc();
//Closures - children always have access to parent scope
```

```js
//Currying - converting functions that take multiple arguments
// into those that take args one at a time
const multiply = (a, b) => a * b;
const curriedMultiply = (a) => (b) => a * b;
// running in console
multiply(3,4)
12
curriedMultiply(3)(4)
12

// Compose - the output of two functions are used in a third
const compose = (f, g) => (a) => f(g(a));
const sum = (num) => num + 1;
compose(sum, sum)(5);  // returns 7

// Avoiding Side Effects, Functional Purity (always returning)
// *Deterministic = same input always produces same output
```

---

## 204: Advanced Arrays

```js
const array = [1, 2, 10, 16];
const double = []
const newArray = array.forEach((num) => {
  double.push(num * 2);
});
console.log('forEach', double);
// output
//(4) [2, 4, 20, 32]
```

- Use map for simple loops
- Map always expects to return an element
- Map creates a new array
- Map reduces side effects

### map

[w3schools map](https://www.w3schools.com/jsref/jsref_map.asp)

```js
const mapArray = array.map((num) => {
  return num * 2
});
console.log('map', mapArray);
// output
//(4) [2, 4, 20, 32]

// can shorten to this if only one variable
const mapArray = array.map(num => num * 2);
```

### filter

```js
const filterArray = array.filter(num => {
  return num > 5
});
console.log('filter', filterArray);
// output
// filter (2) [10, 16]
```

### reduce

```js
const reduceArray = array.reduce((accumulator, num) => {
  return accumulator + num // adds num to the accumulator
}, 0); // starts at 0
console.log('reduce', reduceArray);
// output
// reduce 29
```

---

## 205: Advanced Objects

![image](images/s_FAECDB49EF29D21ADD9E5C332B3EBD7E7D22EDC5CFF96D9D0690BDE774508B24_1558222261654_image.png)

### Reference type

```js
var object1 = { value: 10 };
var object2 = object1;
var object3 = { value: 10 };
object1 === object2 // true
object1 === object3 // false
// numbers, booleans, strings, etc are all primitive types
// reference types simply point to a value
```

### Context vs Scope

```js
console.log(this); // your location - the object you are in
this.alert("hello"); // same as window.alert, because I am currently in the window
const object4 = {
  a: function() {
    console.log(this);
  }
} // now undefined
```

### Instantiation

```js
// when you make a copy of an object and reuse the code
class Player {
  constructor(name, type) {
    console.log('player', this);
    this.name = name;
    this.type = type;
  }
  introduce() {
    console.log(`Hi I am ${this.name}, I'm a ${this.type}`);
  }
}
class Wizard extends Player {
  constructor(name, type) {
    super(name, type);
    console.log('wizard', this);
  }
  play() {
    console.log(`WEEEE I'm a ${this.type}`);
  }
}
const wizard1 = new Wizard('Shelly', 'Healer'); // name, type
const wizard2 = new Wizard('Shawn', 'Dark Magic');
```

---

## 206: ES7

### `.includes` and exponents

```js
'Hellooooooo'.includes('o');
// true
const pets = ['cat', 'dog', 'bat']
pets.includes('dog');
// true
pets.includes('bird');
// false

const square = (x) => x**2
square(7);
// 49
const cube = (x) => x**3
cube(3);
// 27
```

---

## 207: ES8

```js
// #1) Line up the Turtle and the Rabbit at the start line:
const startLine = '     ||<- Start line';
let turtle = '🐢';
let rabbit = '🐇';
startLine
turtle = turtle.padStart(7);
rabbit = rabbit.padStart(7);
// it should look like this:
'     ||<- Start line'
'       🐢'
'       🐇'
// when you do:
console.log(startLine);
console.log(turtle);
console.log(rabbit);

// #2) What happens when you run turtle.trim().padEnd(9, '=') on the turtle variable
// Read about what the second parameter does in padEnd and padStart
turtle = turtle.trim().padEnd(9, '=');

// #3) Get the below object to go from:
let obj = {
  my: 'name',
  is: 'Rudolf',
  the: 'raindeer'
}
// to this:
'my name is Rudolf the raindeer'
Object.entries(obj).map(value => value.join(" ")).join(' ')
// the first join is for values[0] and [1], the second is between each line
```

---

## 208: Advanced Loops

```js
const basket = ['apples', 'oranges', 'grapes']
// all of these return the same thing
//1
for ( let i = 0; i < basket.length; i++) {
  console.log(basket[i]);
}

//2
basket.forEach(item => {
  console.log(item);
})

// for of -> Iterating arrays, strings
for (item of basket) {
  console.log(item);
}

// for in - properties
const detailedBasket = {
  apples: 5,
  oranges: 10,
  grapes: 1000
}
  // properties of an object are enumerable
  // this is enumeration, not iteration
for (item in detailedBasket) {
  console.log(item);
}
// for/of can NOT be run on properties, but for/in CAN be used on itereables. It will return the index
```

## 209: Debugging

```js
const flattened = [[0,1], [2,3], [4,5]].reduce(
  (a, b) => a.concat(b), []);

const flattened = [[0,1], [2,3], [4,5]].reduce(
  (accumulator, array) => {
    console.log('array', array);
    console.log('accumulator', accumulator);
    return accumulator.concat(array)
}, []);

const flattened = [[0,1], [2,3], [4,5]].reduce(
  (accumulator, array) => {
    debugger;
    return accumulator.concat(array)
}, []);
```

---

## 210: Modules

[Medium article on modules](https://medium.com/sungthecoder/javascript-module-module-loader-module-bundler-es6-module-confused-yet-6343510e7bde)

---
