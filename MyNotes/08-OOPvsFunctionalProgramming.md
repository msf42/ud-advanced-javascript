# Section 8: OOP vs Functional Programming

## 126: Composition vs Inheritance

Lots of info. Still not sure I understand the nuances.

---

## 127 and 128: OOP vs FP

Programming Paradigm - Writing code compliant with a specific set of rules

While some languages prefer one over the other, JavaScript allows both.

|                             OOP                              |                        FP                        |
| :----------------------------------------------------------: | :----------------------------------------------: |
|                  Organizing code into units                  | Avoiding side effects and writing pure functions |
| An object is a box containing information and operations that refer to the same concept attributes (or state), methods |        Code is a combination of functions        |
|                                                              |       Data is immutable (no side effects)        |

### Key Differences

|                       OOP                        |                         FP                         |
| :----------------------------------------------: | :------------------------------------------------: |
|          Few operations on common data           |    Many operations for which the data is fixed     |
|           Stateful (state is modified)           |                     Stateless                      |
| Side effects - methods manipulate internal state |          Pure, no side effects (ideally)           |
|  Side effects limit use of multiple processors   |      Purity allows code to be run in parallel      |
|                    imperative                    |                    declarative                     |
|      Better for many things with operations      | Works well for high performance and large datasets |
|        Keeps data and operations together        |         Keeps data and operations separate         |

---
