# Notes for Advanced JavaScript Concepts - Zero to Mastery Course

---

- [01 - Intro](01-Intro.md)

- [02 - JavaScript Foundation](02-JavaScriptFoundation.md)

- [03 - JavaScript Foundation II](03-JavaScriptFoundationII.md)

- [04 - Types in JavaScript](04-TypesInJavaScript.md)

- [05 - The 2 Pillars](05-The2Pillars.md)

- [06 - Object Oriented Programming](06-ObjectOrientedProgramming.md)

- [07 - Functional Programming](07-FunctionalProgramming.md)

- [08 - OOP vs Functional Programming](08-OOPvsFunctionalProgramming.md)

- [09 - Asynchronous JavaScript](09-AsynchronousJavaScript.md)

- [10 - Modules in JavaScript](10-ModulesInJavaScript.md)

- [11 - Error Handling](11-ErrorHandling.md)

---

These sections are from the Complete Web Developer Course and are here as a review

- [14 - JavaScript Basics](14-JavaScriptBasics.md)

- [15 - Intermediate JavaScript](15-IntermediateJavaScript.md)

---
