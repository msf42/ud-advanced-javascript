# Section 06: Object Oriented Programming

## 88: Section Overview

Just an intro

---

## 89: OOP and FP

Still not sure I understand this

![image](images/1569511242924.png)

---

## 90: OOP Introduction

- In OOP, an object is a box that contains information and operations that are supposed to refer to the same concept.
- It is about modeling real world objects and relationships

![image](images/1569512711822.png)

### Example: Dragon Function

- Contains both **data** (a.k.a. "state") and **methods/actions** the dragon can take (sing).
  - These attributes/properties allow us to keep track of the state of the object
  - These methods allow us to manipulate those attributes
- All of this is wrapped in the Dragon function (object).

### Two Main Types

- Class Based

- Prototype Based

---

## 91: OOP1: Factory Functions

### A naive approach:

```js
const elf = {
  name: 'Orwell',
  weapon: 'bow',
  attack() {
    return 'attack with ' + elf.weapon
  }
}

elf.name // 'Orwell'
elf.attack() // 'attack with bow'
```

We could copy and paste this, then change the name, etc.

This is a start, as it includes basic OOP.

### A better way: Factory Functions

```js
function createElf(name, weapon) {
  return {
    name: name, // as of ES6, we can just write name,
    weapon: weapon,
    attack() {
      return 'attack with ' + weapon
    }
  }
}

const peter = createElf('Peter', 'stones')
peter.attack() // 'attack with stones'

const sam = createElf('Sam', 'fire')
sam.attack() // 'attack with fire'
```

---

## 92: OOP2: `Object.create()`

```js
const elfFunctions = {
  attack() {
      return 'attack with ' + this.weapon
    }
}

function createElf(name, weapon) {
  let newElf = Object.create(elfFunctions) // prototypal inheritance
  newElf.name = name;
  newElf.weapon = weapon;
  return newElf;
}

const peter = createElf('Peter', 'stones')
console.log(peter.attack()) // 'attack with stones'

const sam = createElf('Sam', 'fire')
console.log(sam.attack()) // 'attack with fire'
```

This is an improvement, but it isn't best practice.

---

## 93: OOP3: Constructor Functions

Before `Object.create()` ....

```js
function Elf(name, weapon) {
  this.name = name;
  this.weapon = weapon;
}
// this allows peter and sam to both use 'attack' from the same memory location, without having to copy it
Elf.prototype.attack = function() {
  return 'attack with ' + this.weapon
}

// any function invoked with the 'new' keyword is a constructor
const peter = new Elf('Peter', 'stones')
console.log(peter.attack()) // 'attack with stones'

const sam = new Elf('Sam', 'fire')
console.log(sam.attack()) // 'attack with fire'
```

---

## 94: More Constructor Functions

Basically just said that using `new` and constructor functions is preferable to `Object.create()`

---

## 95: Funny Thing About JS...

```js
var a = new Number(5)
a // [Number: 5]
typeof a // object

var b = 5;
b // 5
typeof b // Number

a === b // false
a == b // true
```

---

## 96: OOP4: ES6 Classes

```js
class Elf {
  constructor(name, weapon) {
    this.name = name;
    this.weapon = weapon;
  }
  attack() {  // attack is outside the constructor, because it isn't used by all, and can be shared by all
    return 'attack with ' + this.weapon
  }
}

const peter = new Elf('Peter', 'stones')
console.log(peter.attack()) // 'attack with stones'

const sam = new Elf('Sam', 'fire')
console.log(sam.attack()) // 'attack with fire'

```

Not true classes!

---

## 97: `Object.create()` vs Class

Matter of preference, but Class is becoming more common.

---

## 98: `this` - 4 Ways

```js
// new binding this
// the 'new' word allows us to bind 'this' to the new person
function Person(name, age) {
  this.name = name;
  this.age = age;
}

const person1 = new Person('Xavier', 55)
console.log(person1); //Person { name: 'Xavier', age: 55 }

// implicit binding
// 'this' works as it is programmed
const person = {
  name: 'Karen',
  age: 40,
  hi() {
    console.log('hi' + this.name)
  }
}

console.log(person); // { name: 'Karen', age: 40, hi: [Function: hi] }

// explicit binding
// using bind, call, or apply to explicitly bind 'this'
const person3 = {
  name: 'Karen',
  age: 40,
  hi: function() {
    console.log('hi' + this.setTimeout)
  }.bind(console)
}

console.log(person3); // { name: 'Karen', age: 40, hi: [Function: bound ] }
console.log(person3.hi()) // hi undefined
console.log(person3.age) // 40

// arrow functions
// lexically scoped, rather than dynamically scoped, as above
const person4 = {
  name: 'Karen',
  age: 40,
  hi: function() {
    var inner = () => {
      console.log('hi ' + this.name)
    }
    return inner()
  }
}

person4.hi() // hi Karen
```

---

## 99: Inheritance

Inheritance is a core aspect of OOP - passing knowledge down

### This method breaks the prototypal chain

```js
class Elf {
  constructor(name, weapon) {
    this.name = name;
    this.weapon = weapon;
  }
  attack() {
    return 'attack with ' + this.weapon;
  }
}

const fiona = new Elf('Fiona', 'ninja stars');
fiona // => Elf { name: 'Fiona', weapon: 'ninja stars' }

const ogre = {...fiona}; // makes a copy, but breaks prototypal chain
ogre // => { name: 'Fiona', weapon: 'ninja stars' }

ogre.__proto__  // => {}
fiona.__proto__ // => Elf {}
fiona === ogre  // => false
ogre.attack()   // => error
```

### This method maintains it

```js
class Character {
  constructor(name, weapon) {
    this.name = name;
    this.weapon = weapon;
  }
  attack() {
    return 'attack with ' + this.weapon;
  }
}

/* states: any time an instance of Elf is run, and it uses a property or a method not found here, look up the chain to Character */
class Elf extends Character {
  constructor(name, weapon, type) {
    super(name, weapon); // calls constructor of superclass
    this.type = type
  }
}

const dobby = new Elf('Dobby', 'cloth', 'house');
dobby // => Elf { name: 'Dobby', weapon: 'cloth', type: 'house' }
dobby.attack(); // => 'attack with cloth'

class Ogre extends Character {
  constructor(name, weapon, color) {
    super(name, weapon);
    this.color = color;
  }
  makeFort() {
    return 'strongest fort in the world made';
  }
}

const shrek = new Ogre('Shrek', 'club', 'green');
shrek // => Ogre { name: 'Shrek', weapon: 'club', color: 'green' }
shrek.makeFort(); // => 'strongest fort in the world made'
```

---

## 100: Inheritance 2

```js
class Character {
  constructor(name, weapon) {
    this.name = name;
    this.weapon = weapon;
  }
  attack() {
    return 'attack with ' + this.weapon;
  }
}

/* states: any time an instance of Elf is run, 
  and it uses a property or a method not found
  here, look up the chain to Character */
class Elf extends Character {
  constructor(name, weapon, type) {
    super(name, weapon); // calls constructor of superclass
    this.type = type
  }
}

const dobby = new Elf('Dobby', 'cloth', 'house');
dobby // => Elf { name: 'Dobby', weapon: 'cloth', type: 'house' }
dobby.attack(); // => 'attack with cloth'

class Ogre extends Character {
  constructor(name, weapon, color) {
    super(name, weapon);
    this.color = color;
  }
  makeFort() {
    return 'strongest fort in the world made';
  }
}

const shrek = new Ogre('Shrek', 'club', 'green');
shrek // => Ogre { name: 'Shrek', weapon: 'club', color: 'green' }
shrek.makeFort(); // => 'strongest fort in the world made'

Ogre.isPrototypeOf(shrek); // => false
Ogre.prototype.isPrototypeOf(shrek); // => true
Character.prototype.isPrototypeOf(shrek); // => true
Character.prototype.isPrototypeOf(Ogre); // => false
Character.prototype.isPrototypeOf(Ogre.prototype); // => true
// Character.prototype => Ogre.prototype => shrek

// A better way to check
dobby instanceof Elf; // => true
dobby instanceof Character; // => true
```

In JavaScript, these are just objects, not true classes.

---

## 101: Public vs Private

Doesn't work quite the same in JavaScript as in some other languages. For now, they can't be private.

---

## 102: OOP in React.js

Just an example of OOP in React.

---

## 103: 4 Pillars of OOP

### Encapsulation

OOP encapsulates things into units - wrapping code into boxes.

### Abstraction

Hiding the complexity from the user; creating simpler interfaces.

### Inheritance

Saves memory with shared methods. Avoids rewriting code.

### Polymorphism

The ability to call the same method on different objects and having them respond in different ways.

Such as `Ogre.attack()` and `Elf.attack()` giving different results, even though they were both created using the same class.

---

## 104: Exercise: OOP and Polymorphism

```js
class Character {
  constructor(name, weapon) {
    this.name = name;
    this.weapon = weapon;
  }
  attack() {
    return 'atack with ' + this.weapon
  }
}

class Queen extends Character {
  constructor(name, weapon, type) {
    super(name, weapon)
    this.type = type;
  }
  attack() {
    console.log(super.attack());
    return `I am the ${this.name} of ${this.type}, now bow down to me!`
  }
}
//Polymorphism--
//Extend the Character class to have a Queen class. The output of the below code should be:
const victoria = new Queen('Victoria', 'army', 'hearts'); // create a new instace with the queen having (name, weapon, type). Type inlcudes: 'hearts', 'clubs', 'spades', 'diamonds'

victoria.attack() // will console.log the attack() method in Character class AND will return another string: 'I am the Victoria of hearts, now bow down to me! '
```

---

## 105: Reviewing OOP

![image](images/1569958414469.png)

---
